#!/usr/bin/env python3

# to override print <= can be a big problem with exceptions
from __future__ import print_function # must be 1st
import builtins

import sys

from fire import Fire

from raspipack.version import __version__
from raspipack import unitname
from raspipack import config

from pysyncobj import SyncObj
from pysyncobj.batteries import ReplCounter, ReplDict

import time

class Bcolors:
    HEADER = '[95m'
    OKBLUE = '[94m'
    OKGREEN = '[92m'
    WARNING = '[93m'
    FAIL = '[91m'
    ENDC = '[0m'
    BOLD = '[1m'
    UNDERLINE = '[4m'


def main(port ,  debug=False):
    ''' Main function of the project
    '''
    if not debug:
        _print = print # keep a local copy of the original print
        builtins.print =lambda *args, **kwargs:  None  if (isinstance(args[0], str)) and (args[0].find("D...")==0) else  _print( *args, **kwargs) if ('file' in kwargs) else _print( "{}".format(Bcolors.FAIL   if ((isinstance(args[0], str)) and (args[0].find("X...")>=0)) else Bcolors.ENDC) , *args, Bcolors.ENDC, **kwargs, file=sys.stderr)
    else:
        # debug - show all + colors
        _print = print # keep a local copy of the original print
        builtins.print =lambda *args, **kwargs:   _print( *args, **kwargs) if ('file' in kwargs) else _print( "{}".format(Bcolors.FAIL   if ((isinstance(args[0], str)) and (args[0].find("X...")>=0)) else Bcolors.OKGREEN if  ((isinstance(args[0], str)) and (args[0].find("i...")>=0)) else Bcolors.ENDC  ), *args, Bcolors.ENDC, **kwargs, file=sys.stderr)

    #======== DEFINE THE CONFIG FILE HERE ========

    config.CONFIG['filename'] = "~/.config/raspipack/cfg.json"
    # config.load_config()
    # config.save_config()

    print("i...  script; version:",__version__)
    # print("i... testing info  message",1,2)
    # print("D... testing debug message",3,4)
    # print("X... testing alert message",5,6)
    # print(7,8)
    if port == "usage":
        print(''' ... usage:
        	 _
        ''')
        sys.exit(0)

    elif port == "test":
        print("Ahoj")
        sys.exit(0)

    else:

        if int(port) == 4321:
            p1,p2 = 4321,4322
        else:
            p1,p2 = 4322,4321


        print(p1,p2)

        counter1 = ReplCounter()
        counter2 = ReplCounter()
        dict1 = ReplDict()
        syncObj = SyncObj('127.0.0.1:'+str(p1), [ ], consumers=[counter1, counter2, dict1] )


        print("i... syncobj done, counter sync1", p1)
        counter1.set(p1, sync=True) # set initial value to 42, 'sync' means that operation is blocking

        print("i...               counter sync2", counter1.get())
        counter1.add(1000, sync=True) # add 10 to counter value


        print("i...               counter2 sync", counter1.get(), counter2.get())
        counter2.inc(sync=True) # increment counter value by one

        print("i...               dict sync", counter1.get(),  counter2.get())
        dict1.set('testKey1', 'testValue1', sync=True)

        dict1['testKey2'] = 'testValue2' # this is basically the same as previous, but asynchronous (non-blocking)

        time.sleep(3)
        print("i... final line",counter1.get(), counter2.get(), dict1['testKey1'], dict1.get('testKey2'))




if __name__=="__main__":
    Fire(main)
